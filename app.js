const ex = require('express');
const app = ex();
const router = require('./routes/routes');


app.set("view engine", "ejs");
app.get('/',(req,res)=>{
  res.send("working");
});
app.get("/home", (req, res) => {
  res.render("home");
});
app.use('/api',router);

module.exports=app;
