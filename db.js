
const config = require('./config')
var mysql = require("mysql");
exports.pool = mysql.createPool({
  host: config.DB_HOST,
  user: config.USERNAME,
  port: config.DB_PORT,
  password: config.DB_PASSWORD,
  database: config.DB_NAME,
  connectionLimit : 10,
  multipleStatements: true
});