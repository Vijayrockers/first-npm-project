const http = require('http');
const app = require('./app');
const config = require('./config');

const server =http.createServer(app);
server.listen(process.env.PORT || 3000,(err)=>{
  if(!err){
    console.log('Server Running on port '+3000);
  }
  else{
    console.log('Error while listening to server');
  }
});