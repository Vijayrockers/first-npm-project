const e = require("express");
const db = require("../db");
exports.getpost = (req, res, next) => {
  if (req.params.id != null) {
    const limit = 100;
    const page = req.query.page;
    const offset = (page - 1) * limit;
    db.pool.query(
      "SELECT * from post where id=" + req.params.id,
      (err, rows, fields) => {
        if (err) throw err;
        res.status(200).json({ data: rows });
      }
    );
  } else {
    return res.status(203).json({
      error: "No Data",
    });
  }
};

exports.getpostpage = (req, res, next) => {
  if (req.query.page != null) {
    const limit = 6;
    const page = req.query.page;
    const offset = (page - 1) * limit;
    db.pool.query(
      "SELECT * from post order by id desc limit " + limit + " offset " + offset,
      (err, rows, fields) => {
        if (err) throw err;
        res.status(200).json({ data: rows });
      }
    );
  } else {
    return res.status(203).json({
      error: "No Data",
    });
  }
};

exports.getramdompost = (req, res, next) => {
  db.pool.query(
    "SELECT * from post order by rand() limit 3 ",
    (err, rows, fields) => {
      if (err) throw err;
      res.status(200).json({ data: rows });
    }
  );
};

exports.searchpost = (req, res, next) => {
  const title = decodeURI(req.params.title);
  const page = req.query.page;
  const limit = 5;
  const offset = (page - 1) * limit;
  db.pool.query(
    "SELECT * from post where title like '%" +
      title +
      "%'  order by id desc limit " +
      limit +
      " offset " +
      offset,
    (err, rows, fields) => {
      if (err) throw err;
      res.status(200).json({ data: rows });
    }
  );
};

exports.classpost = (req, res, next) => {
  const id = req.params.id;
  const page = req.query.page;
  const limit = 7;
  const offset = (page - 1) * limit;
  db.pool.query(
    "SELECT std_subject.id,post.* from post join std_subject on post.class_subject=std_subject.id where std_subject.std_id= " +
      id +
      " and enabled=1 order by post.id desc limit " +
      limit +
      " offset " +
      offset,
    (err, rows, fields) => {
      if (err) throw err;
      res.status(200).json({ data: rows });
    }
  );
};

