const jwt = require('jsonwebtoken');

const config = require ('./config');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.token;
    if (token !== config.API_TOKEN) {
      throw 'Invalid user ID';
    } else {
      next();
    }
  } catch {
    res.status(401).json({
      error: 'Invalid request!'
    });
  }
};