const router = require('express').Router();
const auth = require('../auth');


const HomeController = require('../controller/homeController');
const PostController = require('../controller/postController');
const ClassController = require('../controller/classController');
const Config = require('../config');

//router.get('/getdata',auth,apicontroller.getdata);
router.get('/ads',HomeController.getadsdata);

router.get('/gethome',HomeController.gethome);

router.get('/getpost/:id', PostController.getpost);
router.get('/getpost', PostController.getpostpage);
router.get('/getrandom', PostController.getramdompost);
router.get('/searchpost/:title', PostController.searchpost);
router.get('/getclasspost/:id', PostController.classpost);


router.get('/getallclass/', ClassController.getallclass);


module.exports=router;